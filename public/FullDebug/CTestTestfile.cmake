# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce
# Build directory: /builds/felisce/felisce/build/FullDebug
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("external/doctest")
subdirs("external/bitmap")
subdirs("external/libMeshb")
subdirs("src")
subdirs("tests")
