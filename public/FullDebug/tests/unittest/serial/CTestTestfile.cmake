# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests/unittest/serial
# Build directory: /builds/felisce/felisce/build/FullDebug/tests/unittest/serial
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(externalCurveGeneratorAdvanced "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/run_test.py")
set_tests_properties(externalCurveGeneratorAdvanced PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(externalCurveGeneratorBasic "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/run_test.py")
set_tests_properties(externalCurveGeneratorBasic PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(externalsciplotsciplot "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/run_test.py")
set_tests_properties(externalsciplotsciplot PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcCorearray_1d "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/run_test.py")
set_tests_properties(srcCorearray_1d PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcCorechrono "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/run_test.py")
set_tests_properties(srcCorechrono PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcCorefelisceParam "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/run_test.py")
set_tests_properties(srcCorefelisceParam PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcCorefilesystemUtil "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/run_test.py")
set_tests_properties(srcCorefilesystemUtil PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcCoreindexed_object "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/run_test.py")
set_tests_properties(srcCoreindexed_object PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcFiniteElementcurBaseFiniteElement "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/run_test.py")
set_tests_properties(srcFiniteElementcurBaseFiniteElement PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcFiniteElementelementMatrix "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/run_test.py")
set_tests_properties(srcFiniteElementelementMatrix PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcFiniteElementelementVector "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/run_test.py")
set_tests_properties(srcFiniteElementelementVector PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcGeometrygeometricMeshRegion "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/run_test.py")
set_tests_properties(srcGeometrygeometricMeshRegion PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcGeometryinterpolation "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/run_test.py")
set_tests_properties(srcGeometryinterpolation PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcGeometryintersection "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/run_test.py")
set_tests_properties(srcGeometryintersection PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcGeometrylocalization "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/run_test.py")
set_tests_properties(srcGeometrylocalization PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcGeometrypoint "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/run_test.py")
set_tests_properties(srcGeometrypoint PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcHyperelasticityLawsHyperElasticLaw "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/run_test.py")
set_tests_properties(srcHyperelasticityLawsHyperElasticLaw PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcHyperelasticityLawsOgden "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/run_test.py")
set_tests_properties(srcHyperelasticityLawsOgden PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcHyperelasticityLawsciarletGeymonat "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/run_test.py")
set_tests_properties(srcHyperelasticityLawsciarletGeymonat PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcHyperelasticityLawsstVenantKirchhoff "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/run_test.py")
set_tests_properties(srcHyperelasticityLawsstVenantKirchhoff PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcInputOutputio "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/run_test.py")
set_tests_properties(srcInputOutputio PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolscontour_utilities "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/run_test.py")
set_tests_properties(srcToolscontour_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolsfe_utilities "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/run_test.py")
set_tests_properties(srcToolsfe_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolsmath_utilities "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/run_test.py")
set_tests_properties(srcToolsmath_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolssciplot_utilities "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/run_test.py")
set_tests_properties(srcToolssciplot_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolstest_table_interpolation "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/run_test.py")
set_tests_properties(srcToolstest_table_interpolation PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(srcToolstest_utilities "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/run_test.py")
set_tests_properties(srcToolstest_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;102;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(Pythontest_generate_contour "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_generate_contour/test_generate_contour.py")
set_tests_properties(Pythontest_generate_contour PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_generate_contour" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;168;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(Pythontest_hello_world "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_hello_world/test_hello_world.py")
set_tests_properties(Pythontest_hello_world PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_hello_world" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;168;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
add_test(Pythontest_mesh2ensight "python3" "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_mesh2ensight/test_mesh2ensight.py")
set_tests_properties(Pythontest_mesh2ensight PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcExecutable/test_mesh2ensight" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;168;ADD_TEST;/builds/felisce/felisce/tests/unittest/serial/CMakeLists.txt;0;")
