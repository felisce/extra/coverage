# Install script for directory: /builds/felisce/felisce/tests/unittest/serial

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/builds/felisce/felisce/bin/FullDebug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "FullDebug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestexternalCurveGeneratorAdvanced")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Advanced/TestexternalCurveGeneratorAdvanced")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestexternalCurveGeneratorBasic")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalCurveGenerator/Basic/TestexternalCurveGeneratorBasic")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/Testexternalsciplotsciplot")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalsciplot/sciplot/Testexternalsciplotsciplot")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcCorearray_1d")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/array_1d/TestsrcCorearray_1d")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcCorechrono")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/chrono/TestsrcCorechrono")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcCorefelisceParam")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/felisceParam/TestsrcCorefelisceParam")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcCorefilesystemUtil")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/filesystemUtil/TestsrcCorefilesystemUtil")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcCoreindexed_object")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/indexed_object/TestsrcCoreindexed_object")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcFiniteElementcurBaseFiniteElement")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/curBaseFiniteElement/TestsrcFiniteElementcurBaseFiniteElement")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcFiniteElementelementMatrix")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementMatrix/TestsrcFiniteElementelementMatrix")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcFiniteElementelementVector")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcFiniteElement/elementVector/TestsrcFiniteElementelementVector")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcGeometrygeometricMeshRegion")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/geometricMeshRegion/TestsrcGeometrygeometricMeshRegion")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcGeometryinterpolation")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/interpolation/TestsrcGeometryinterpolation")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcGeometryintersection")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/intersection/TestsrcGeometryintersection")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcGeometrylocalization")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/localization/TestsrcGeometrylocalization")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcGeometrypoint")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcGeometry/point/TestsrcGeometrypoint")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcHyperelasticityLawsHyperElasticLaw")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/HyperElasticLaw/TestsrcHyperelasticityLawsHyperElasticLaw")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcHyperelasticityLawsOgden")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/Ogden/TestsrcHyperelasticityLawsOgden")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcHyperelasticityLawsciarletGeymonat")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/ciarletGeymonat/TestsrcHyperelasticityLawsciarletGeymonat")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcHyperelasticityLawsstVenantKirchhoff")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcHyperelasticityLaws/stVenantKirchhoff/TestsrcHyperelasticityLawsstVenantKirchhoff")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcInputOutputio")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcInputOutput/io/TestsrcInputOutputio")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolscontour_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/contour_utilities/TestsrcToolscontour_utilities")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolsfe_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/fe_utilities/TestsrcToolsfe_utilities")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolsmath_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/math_utilities/TestsrcToolsmath_utilities")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolssciplot_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/sciplot_utilities/TestsrcToolssciplot_utilities")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolstest_table_interpolation")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_table_interpolation/TestsrcToolstest_table_interpolation")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/serial/TestsrcToolstest_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/test_utilities/TestsrcToolstest_utilities")
    endif()
  endif()
endif()

