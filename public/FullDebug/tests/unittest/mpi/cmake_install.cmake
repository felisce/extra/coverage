# Install script for directory: /builds/felisce/felisce/tests/unittest/mpi

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/builds/felisce/felisce/bin/FullDebug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "FullDebug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScbcgs")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs/MPITestexternalPETScbcgs")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScbcgs_asm")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_asm/MPITestexternalPETScbcgs_asm")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScbcgs_cholesky")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_cholesky/MPITestexternalPETScbcgs_cholesky")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScbcgs_jacobi")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_jacobi/MPITestexternalPETScbcgs_jacobi")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScbcgs_lu")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/bcgs_lu/MPITestexternalPETScbcgs_lu")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSccg")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg/MPITestexternalPETSccg")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSccg_asm")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_asm/MPITestexternalPETSccg_asm")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSccg_cholesky")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_cholesky/MPITestexternalPETSccg_cholesky")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSccg_jacobi")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_jacobi/MPITestexternalPETSccg_jacobi")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSccg_lu")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/cg_lu/MPITestexternalPETSccg_lu")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScgmres")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres/MPITestexternalPETScgmres")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScgmres_asm")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_asm/MPITestexternalPETScgmres_asm")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScgmres_cholesky")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_cholesky/MPITestexternalPETScgmres_cholesky")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScgmres_jacobi")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_jacobi/MPITestexternalPETScgmres_jacobi")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScgmres_lu")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/gmres_lu/MPITestexternalPETScgmres_lu")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSchypre_euclid")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_euclid/MPITestexternalPETSchypre_euclid")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETSchypre_parasails")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/hypre_parasails/MPITestexternalPETSchypre_parasails")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScmumps")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/mumps/MPITestexternalPETScmumps")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestexternalPETScsuperludist")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/externalPETSc/superludist/MPITestexternalPETScsuperludist")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestsrcCorempiInfo")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcCore/mpiInfo/MPITestsrcCorempiInfo")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestsrcModelheatModel")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcModel/heatModel/MPITestsrcModelheatModel")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestsrcPETScInterfacepetscVector")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcPETScInterface/petscVector/MPITestsrcPETScInterfacepetscVector")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestsrcSolverlinearProblemHeat")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcSolver/linearProblemHeat/MPITestsrcSolverlinearProblemHeat")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/unittest/mpi/MPITestsrcToolspetsc_utilities")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:/builds/felisce/felisce/build/FullDebug/external/doctest:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/tests/srcTools/petsc_utilities/MPITestsrcToolspetsc_utilities")
    endif()
  endif()
endif()

