# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests/unittest
# Build directory: /builds/felisce/felisce/build/FullDebug/tests/unittest
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("mpi")
subdirs("serial")
