# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests
# Build directory: /builds/felisce/felisce/build/FullDebug/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("simulation_tests")
subdirs("simulation_tests_fsi")
subdirs("unittest")
subdirs("felisce-ns")
