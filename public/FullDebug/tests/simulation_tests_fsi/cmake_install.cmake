# Install script for directory: /builds/felisce/felisce/tests/simulation_tests_fsi

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/builds/felisce/felisce/bin/FullDebug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "FullDebug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/simulation_tests_fsi/NS_ALE-pvm")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-pvm/NS_ALE-pvm")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq"
         RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq" TYPE EXECUTABLE FILES "/builds/felisce/felisce/build/FullDebug/tests/simulation_tests_fsi/NS_ALE-zmq")
  if(EXISTS "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq"
         OLD_RPATH "/builds/felisce/felisce/build/FullDebug/src:/builds/felisce/felisce/build/FullDebug/external/libMeshb:/builds/felisce/felisce/build/FullDebug/external/bitmap:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/builds/felisce/felisce/external/Xfm/lib/Ubuntu22:/home/felisce/cvgraph/lib:"
         NEW_RPATH "/builds/felisce/felisce/bin/FullDebug/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/home/felisce/src/petsc/linux-gnu/lib:/home/felisce/cvgraph/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/builds/felisce/felisce/bin/FullDebug/bin/fsi/Fluid/NS_ALE-zmq/NS_ALE-zmq")
    endif()
  endif()
endif()

