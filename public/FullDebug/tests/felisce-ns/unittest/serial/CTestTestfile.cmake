# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests/felisce-ns/unittest/serial
# Build directory: /builds/felisce/felisce/build/FullDebug/tests/felisce-ns/unittest/serial
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(NSsrcSolverlinearProblem3DBeam "/usr/bin/python3.10" "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcSolver/linearProblem3DBeam/run_test.py")
set_tests_properties(NSsrcSolverlinearProblem3DBeam PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcSolver/linearProblem3DBeam" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;96;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;0;")
add_test(NSsrcSolverlinearProblem3DShell "/usr/bin/python3.10" "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcSolver/linearProblem3DShell/run_test.py")
set_tests_properties(NSsrcSolverlinearProblem3DShell PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcSolver/linearProblem3DShell" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;96;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;0;")
add_test(NSsrcToolsextruding_utilities "/usr/bin/python3.10" "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcTools/extruding_utilities/run_test.py")
set_tests_properties(NSsrcToolsextruding_utilities PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcTools/extruding_utilities" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;96;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;0;")
add_test(NSPythontest_collapse "/usr/bin/python3.10" "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcExecutable/test_collapse/test_collapse.py")
set_tests_properties(NSPythontest_collapse PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcExecutable/test_collapse" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;158;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;0;")
add_test(NSPythontest_extrude "/usr/bin/python3.10" "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcExecutable/test_extrude/test_extrude.py")
set_tests_properties(NSPythontest_extrude PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/srcExecutable/test_extrude" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;158;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/unittest/serial/CMakeLists.txt;0;")
