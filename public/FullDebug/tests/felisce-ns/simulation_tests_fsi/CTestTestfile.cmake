# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi
# Build directory: /builds/felisce/felisce/build/FullDebug/tests/felisce-ns/simulation_tests_fsi
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(NSFSIPressureWaveBeam_pvm "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSIPressureWaveBeam_pvm PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSIPressureWaveBeam_pvm" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
add_test(NSFSIPressureWaveBeam_zmq "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSIPressureWaveBeam_zmq PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSIPressureWaveBeam_zmq" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
add_test(NSFSIPressureWaveShell_pvm "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSIPressureWaveShell_pvm PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSIPressureWaveShell_pvm" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
add_test(NSFSIPressureWaveShell_zmq "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSIPressureWaveShell_zmq PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSIPressureWaveShell_zmq" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
add_test(NSFSICavityFlexibleBottomBeam_pvm "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSICavityFlexibleBottomBeam_pvm PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSICavityFlexibleBottomBeam_pvm" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
add_test(NSFSICavityFlexibleBottomBeam_zmq "/usr/bin/python3.10" "run_test_fsi.py")
set_tests_properties(NSFSICavityFlexibleBottomBeam_zmq PROPERTIES  WORKING_DIRECTORY "/builds/felisce/felisce/bin/FullDebug/bin/tests_ns/FSICavityFlexibleBottomBeam_zmq" _BACKTRACE_TRIPLES "/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;139;ADD_TEST;/builds/felisce/felisce/tests/felisce-ns/simulation_tests_fsi/CMakeLists.txt;0;")
