# CMake generated Testfile for 
# Source directory: /builds/felisce/felisce/tests/felisce-ns
# Build directory: /builds/felisce/felisce/build/FullDebug/tests/felisce-ns
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("simulation_tests")
subdirs("simulation_tests_fsi")
subdirs("unittest")
