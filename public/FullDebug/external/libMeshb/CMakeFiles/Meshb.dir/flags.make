# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# compile C with /usr/bin/gcc
C_DEFINES = -DCURVEGEN_DIR=\"/builds/felisce/felisce/external/CurveGenerator/\" -DFELISCE_COMPILED_IN_LINUX -DFELISCE_WITH_CURVEGEN -DFELISCE_WITH_CVGRAPH -DFELISCE_WITH_LIBXFM -DFELISCE_WITH_LUA -DFELISCE_WITH_NSINRIA -DFELISCE_WITH_PVM -DFELISCE_WITH_SLEPC -DFULL_PETSC_CODE -DHYPRE_COMPILED -DMeshb_EXPORTS -DSUPERLU_DIST_COMPILED

C_INCLUDES = -I/builds/felisce/felisce/external/libMeshb/libMeshb/sources -isystem /usr/lib/x86_64-linux-gnu/openmpi/include -isystem /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi

C_FLAGS = --coverage -funroll-loops -Wall -fPIC

