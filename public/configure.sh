#!/bin/sh

# get the number of physical threads for ubuntu (MAC OS X ???)
# nproc is the same value as OMP_NUM_THREADS
export FELISCE_ALL_THREADS=$(nproc)
export FELISCE_ALL_THREADS_MINUS_ONE=$(($(nproc)-1))

echo "FELISCE_ALL_THREADS=${FELISCE_ALL_THREADS}"
echo "FELISCE_ALL_THREADS_MINUS_ONE=${FELISCE_ALL_THREADS_MINUS_ONE}"

# Prepare git
python3 ../scripts/utils/git_submodules_prepare.py

# Ensuring that everything is up-to-date
git submodule sync --recursive
git submodule update --init --recursive
git submodule update --remote

# Set variables
export FELISCE_SOURCE="${FELISCE_SOURCE:-"$( cd "$(dirname "$0")" ; pwd -P )"/..}"
export FELISCE_BUILD="${FELISCE_SOURCE}/build"

# Set basic configuration
export FELISCE_BUILD_TYPE=${FELISCE_BUILD_TYPE:-"FullDebug"}

# Auxiliary sets
if [ -z "$FELISCE_NS" ] ;then
    export FELISCE_NS=true
fi
if [ -z "$CMAKE_UNITY_BUILD" ] ;then
    export CMAKE_UNITY_BUILD=ON
fi
if [ -z "$FELISCE_WITH_NSINRIA" ] ;then
    export FELISCE_WITH_NSINRIA=true
fi
if [ -z "$BUILD_TESTING" ] ;then
    export BUILD_TESTING=true
fi
if [ -z "$BUILD_NIGHTLY_TESTS" ] ;then
    export BUILD_NIGHTLY_TESTS=true
fi
if [ -z "$JUST_COMPILATION_TESTS" ] ;then
    export JUST_COMPILATION_TESTS=true
fi
if [ -z "$BUILD_EXAMPLES" ] ;then
    export BUILD_EXAMPLES=false
fi
if [ -z "$BUILD_BENCHMARKS" ] ;then
    export BUILD_BENCHMARKS=false
fi
if [ -z "$INSTALL_FELISCE_SYSTEM" ] ;then
    export INSTALL_FELISCE_SYSTEM=false
fi
if [ -z "$BUILD_FOR_CPACK" ] ;then
    export BUILD_FOR_CPACK=false
fi
if [ -z "$FELISCE_TEST_TOLERANCE" ] ;then
    export FELISCE_TEST_TOLERANCE=1.0e-5
fi

# Set compiler
if [ -z "$CC" ] ;then
    export CC=gcc
    export CXX=g++
fi

# Compiler flags
if [ "$CC" = gcc ] ; then
    export FELISCE_CMAKE_CXX_FLAGS="${FELISCE_CMAKE_CXX_FLAGS} -Wignored-qualifiers -Werror=ignored-qualifiers -Werror=suggest-override -Werror=unused-variable -Werror=misleading-indentation -Werror=return-type -Werror=sign-compare -Werror=unused-but-set-variable -Werror=unused-local-typedefs -Werror=reorder -Werror=maybe-uninitialized -Werror=comment -Werror=parentheses"
    #export FELISCE_CMAKE_CXX_FLAGS="${FELISCE_CMAKE_CXX_FLAGS} -Wignored-qualifiers -Werror=ignored-qualifiers -Werror=suggest-override -Werror=unused-variable -Werror=misleading-indentation -Werror=return-type -Werror=sign-compare -Werror=unused-but-set-variable -Werror=unused-local-typedefs -Werror=reorder -Werror=maybe-uninitialized -Werror=comment -Werror=parentheses -Werror=deprecated-declarations"
else
    export FELISCE_CMAKE_CXX_FLAGS="${FELISCE_CMAKE_CXX_FLAGS} -Werror=delete-non-abstract-non-virtual-dtor -Werror=extra-tokens -Werror=defaulted-function-deleted -Werror=potentially-evaluated-expression -Werror=instantiation-after-specialization -Werror=undefined-var-template -Werror=unused-lambda-capture -Werror=unused-const-variable -Werror=parentheses -Werror=pessimizing-move -Werror=logical-op-parentheses"
    #export FELISCE_CMAKE_CXX_FLAGS="${FELISCE_CMAKE_CXX_FLAGS} -Werror=delete-non-abstract-non-virtual-dtor -Werror=extra-tokens -Werror=defaulted-function-deleted -Werror=potentially-evaluated-expression -Werror=instantiation-after-specialization -Werror=undefined-var-template -Werror=unused-lambda-capture -Werror=unused-const-variable -Werror=parentheses -Werror=pessimizing-move -Werror=logical-op-parentheses -Werror=deprecated-declarations"
fi

# Clean
clear
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeFiles"

# Compilation flags
export CMAKE_C_FLAGS="--coverage"
export CMAKE_CXX_FLAGS="--coverage"

# Configure
scan-build -v -plist -o analyzer_reports cmake --no-warn-unused-cli ..                              \
-H"${FELISCE_SOURCE}"                                                                               \
-B"${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}"                                                          \
-DCMAKE_BUILD_TYPE="${FELISCE_BUILD_TYPE}"                                                          \
-DCMAKE_CXX_COMPILER="${CXX}"                                                                        \
-DCMAKE_C_COMPILER="${CC}"                                                                         \
-DCMAKE_CXX_FLAGS="${FELISCE_CMAKE_CXX_FLAGS}"                                                      \
-DCMAKE_UNITY_BUILD=${CMAKE_UNITY_BUILD}                                                            \
-DBUILD_TESTING=ON                                                                                  \
-DOPEN_MPI_RUN="/usr/bin/mpirun"                                                                    \
-DPETSC_DIR="$HOME/src/petsc"                                                                       \
-DFELISCE_WITH_LIBXFM=ON                                                                            \
-DFELISCE_WITH_SLEPC=ON                                                                             \
-DFELISCE_WITH_PVM=ON                                                                              \
-DFELISCE_WITH_ZEROMQ=OFF                                                                            \
-DFELISCE_WITH_LUA=ON                                                                               \
-DFELISCE_WITH_CURVEGEN=ON                                                                          \
-DFELISCE_DATA_DIR="/home/felisce/data/"                                                            \
-DFELISCE_WITH_CVGRAPH=ON                                                                           \
-DCVGRAPH_ROOT_DIR="/home/felisce/cvgraph/"                                                         \
-DFELISCE_WITH_NSINRIA=${FELISCE_WITH_NSINRIA}                                                      \
-DNSINRIA_DIR=${NSINRIA_DIR}                                                                        \
-DFELISCE_WITH_QT=OFF                                                                               \
-DBUILD_NIGHTLY_TESTS=${BUILD_NIGHTLY_TESTS}                                                        \
-DJUST_COMPILATION_TESTS=${JUST_COMPILATION_TESTS}                                                  \
-DINSTALL_FELISCE_SYSTEM=${INSTALL_FELISCE_SYSTEM}                                                  \
-DCMAKE_C_FLAGS=${CMAKE_C_FLAGS}                                                                    \
-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}                                                                \
-DCMAKE_VERBOSE_MAKEFILE=ON                                                                         \
-DCMAKE_EXPORT_COMPILE_COMMANDS=ON                                                                  \
-DFELISCE_TEST_TOLERANCE=${FELISCE_TEST_TOLERANCE}                                                  \
-DMASTERFSI_PVM="/home/felisce/MasterFSIContact/bin/PVM/MasterFSI"                                  \
-DMASTERFSI_ZMQ="/home/felisce/MasterFSIContact/bin/ZMQ/MasterFSI"                                  \

echo "Compile"

scan-build -v -plist -o analyzer_reports cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --target install -- -j$(nproc) 2>&1 | tee felisce-build.log
if [ "$FELISCE_NS" = false ] ; then
    mv felisce-build.log ../
else
    mv felisce-build.log ../../
fi
echo "Build complete"



FILE=${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}/lib/libFELiScE.so
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
    echo "$FILE does not exist."
    exit 1
fi

# J. Morice 10/2023
# fix the number of threads in linux for run the test.
# Rq: coverage is only work on linux computer
export OMP_NUM_THREADS=1

# Running test
echo "Test run"
ctest --output-on-failure

# Generate coverage
echo "Coverage run"
export OMP_NUM_THREADS="${FELISCE_ALL_THREADS}"

if [ "$FELISCE_NS" = false ] ; then
    echo "Coverage run:: Step 1"
    gcovr -b -r ../src --object-directory ./${FELISCE_BUILD_TYPE}
    echo "Coverage run:: Step 2"
    gcovr -b -r ../src --object-directory ./${FELISCE_BUILD_TYPE} --xml-pretty -o coverage.xml
    echo "Coverage run:: Step 3"
    # gcovr -b -r ../src --object-directory ./${FELISCE_BUILD_TYPE} --sonarqube coverage_sonarqube.xml
    gcovr -b -r ../src --object-directory ./${FELISCE_BUILD_TYPE} --html --html-details -o index.html
else
    echo "Coverage run:: Step 1"
    gcovr -b -r ../../src --object-directory ./${FELISCE_BUILD_TYPE}
    echo "Coverage run:: Step 2"
    gcovr -b -r ../../src --object-directory ./${FELISCE_BUILD_TYPE} --xml-pretty -o coverage.xml
    echo "Coverage run:: Step 3"
    # gcovr -b -r ../../src --object-directory ./${FELISCE_BUILD_TYPE} --sonarqube coverage_sonarqube.xml
    gcovr -b -r ../../src --object-directory ./${FELISCE_BUILD_TYPE} --html --html-details -o index.html
fi

# lcov --directory ${FELISCE_BUILD_TYPE} --capture --output-file coverage.lcov
# genhtml -o coverage coverage.lcov

# Refactor coverage
echo "Refactor coverage"
if [ "$FELISCE_NS" = false ] ; then
    python3 ../scripts/utils/rename_relative_paths_coverage.py
else
    python3 ../scripts/utils/rename_relative_paths_coverage_ns.py
fi

# Valgrind
echo "Valgrind"
export OMP_NUM_THREADS=1

valgrind --xml=yes --xml-file=felisce-valgrind.xml --memcheck:leak-check=full --show-reachable=yes  --suppressions=/usr/share/openmpi/openmpi-valgrind.supp "ctest" "10" "10" "200" "0" "0"
if [ "$FELISCE_NS" = false ] ; then
    mv felisce-valgrind.xml ../
else
    mv felisce-valgrind.xml ../../
fi

# Clang-Tidy
echo "Clang-Tidy"
export OMP_NUM_THREADS="${FELISCE_ALL_THREADS}"

mv ./FullDebug/compile_commands.json ./
echo "Remove some object in compile_commands.json to reduce the size of clang-tidy-report"
cp ../scripts/removeDirectoryForClangTidyAnalysis.py .
python3 removeDirectoryForClangTidyAnalysis.py
mv compile_commands.json compile_commands.json.old
mv compile_commands.json.new compile_commands.json

#/usr/lib/llvm-10/share/clang/run-clang-tidy.py -checks='*' -header-filter=.. -p . -j$(nproc) > clang-tidy-report # focal call : default clang, clang-tidy
/usr/lib/llvm-14/bin/run-clang-tidy -checks='*' -header-filter=../src -p . -j$(nproc) > clang-tidy-report       # jammy call : default clang, clang-tidy
if [ "$FELISCE_NS" = false ] ; then
    mv clang-tidy-report ..
    sizeFileReport=$(du -k ../clang-tidy-report | cut -f 1)
    echo "size of report is $sizeFileReport kilobytes"
else
    mv clang-tidy-report ../../
    sizeFileReport=$(du -k ../../clang-tidy-report | cut -f 1)
    echo "size of report is $sizeFileReport kilobytes"
fi
