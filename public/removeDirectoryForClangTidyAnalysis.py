import json

def isNotExternalCompiledLibrary(a):
    if 'external' in a:
        print("external in directory")
    if 'external/doctest' in a:
        return False
    elif 'external/bitmap' in a:
        return False
    elif 'external/libMeshb' in a:
        return False
    else:
        return True
    


with open('compile_commands.json', 'r') as jf:
    jsonFile = json.load(jf)

print('Length of JSON list of object before cleaning: ', len(jsonFile))
print('      ');
print('JSON object [0]: ', jsonFile[0])
print('      ');
print('JSON object [1]: ', jsonFile[1])
print('      ');
print('Length of JSON object before cleaning[0]: ', len(jsonFile[0].keys()))
print('      ');



testJson = [];
#counter = 0
for elem in jsonFile:
    keyList = elem.keys()
    for key in keyList:
        if key.startswith('directory'):
            #print(key)
            if isNotExternalCompiledLibrary(elem[key])== True : 
                testJson.append(elem);
                #counter=counter+1
        
print('Length of JSON list of object after cleaning: ', len(testJson))

with open('compile_commands.json.new', 'w') as jf:
    #json.dumps(testJson, jf)
    jf.write(json.dumps(testJson,indent=0, separators=(',', ': ')))
